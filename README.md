# My Harry Potter Control

Mods to the Harry Potter Lamp project.

Based on the work here: https://github.com/mamacker/pi_to_potter

I want  to  have commands for turn on/off a lamp and unlock a drawer. I intend to have a NeoPixel jewel 
give a visual count down on the unlocked drawer command - 10 seconds to open it or the drawer locks again (to save the solenoid).
The NeoPixel might give other feedback.

Target December 28th 2018 - next Granddaughters visit!!!

Now have Neopixel code working and successfully had a "Spell" work, which means the SSR can switch on/off the light as I need.
Next step is to tidy up my "Spell" casting and add the appropriate GPIO commands!



The is code is running on a Pi Zero under python3 - sudo python3 trainedwpins.py
It takes a while to get running, but that might be the Pi Zero. Might have to try a Pi3 for a quicker operation. (12/23/18)

Failed to make it for New Year as the visitors arrived earlier than expected. Instead had a magnetic Reed switch actuated by a
3D printed Ring with a magnet inside. The ring unlocked the drawer (for 10 seconds) and turned on fairy lights in the shape
of an A (letter of her first name) for 3 minutes. Wiring diagram and code (lock.py) (1/2/19)

