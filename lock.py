# Draw opening and fairy light code
# Dave Robinson


import RPi.GPIO as GPIO
import time
import os
import random
from random import randrange
import warnings
import threading
import sys

MAGNET = 17                     # Set GPIO 17 as input for reed switch
POWER = 23                      # Set GPIO 23 as output for SSR
LOCK = 24                       # Set GPIO 24 as output for Lock

GPIO.setmode(GPIO.BCM)
GPIO.setup(MAGNET, GPIO.IN, GPIO.PUD_UP)
GPIO.setup(LOCK, GPIO.OUT)
GPIO.setup(POWER, GPIO.OUT)
print("Set up Sleep Time")
GPIO.output(POWER, 0)
GPIO.output(LOCK, 0)

time.sleep(10)

light_time = 0
lights_on = False


while True:
#        print("While Loop top")
#        print(GPIO.input(MAGNET))
        if ((GPIO.input(MAGNET)) == 0):  
#                print("Magnet If check")
                GPIO.output(LOCK, 1)
                GPIO.output(POWER, 1)
                lights_on = True
                time.sleep(10)
                GPIO.output(LOCK, 0)
                
# end of MAGNET If loop  
        if lights_on is True: 
            light_time = light_time + 1
#            print("Increment light_time")   
        
# End of lights timer increment        
        if light_time > 300:
            GPIO.output(POWER, 0)
            light_time = 0
            lights_on = False
#            print("Light time expired")
# End of light_timer 5 minutes  

        time.sleep(1)
    
# end of while loop
